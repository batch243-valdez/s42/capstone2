const Cart = require("../models/cartModel")
const auth = require("../auth")
const Product = require("../models/productsModel");
const OrderTransactionHistory = require("../models/orderTransactionHistoryModel");
const User = require("../models/usersModel")
const ObjectId = require('mongoose').Types.ObjectId;

const addProductToCart = async (req, res) => {
    try{
        const productId = req.body.productId;
        if(!ObjectId.isValid(productId)){
            return res.status(400).send(`Product Id ${productId} is not valid objectId!`);
        }

        const token = await req.headers.authorization;
        const userData = await auth.decode(token);
        const productData = await Product.findById(productId);

        if(!productData)
            return res.status(404).send(`Product Id ${productId} is not found!`);
                
        const userCart = await Cart.findOne({userId: userData.id});
        
        if(userData.isAdmin) { return res.send("Admin is not allowed to add products to cart!")}

        const productsToAddInCart = {
            productId: req.body.productId,
            productName: productData.name,
            price: productData.price,
            quantity: req.body.quantity,
            total: productData.price * req.body.quantity
        }

        const addItemToCart = await new Cart({
            userId: userData.id,
            product: productsToAddInCart,
            totalAmountOfAllProducts: productData.price * req.body.quantity
        })

        let arrIndex;

        if(userCart){
            for(let i=0; i < userCart.product.length; i++){
                if (userCart.product[i].productId === req.body.productId){
                    arrIndex = i;
                    break;
                }
            }

            const userCartToBeUpdated = userCart.product[arrIndex];

            if(arrIndex >= 0){
                
                userCartToBeUpdated.quantity += req.body.quantity;
                userCartToBeUpdated.total = userCartToBeUpdated.price * userCartToBeUpdated.quantity;

                let totalAmount = 0;
                userCart.product.forEach(element => {
                    totalAmount += element.total;
                });
                userCart.totalAmountOfAllProducts = totalAmount;
                await userCart.save();

                return res.status(200).json(userCart);
            }
            else{
                await userCart.product.push(productsToAddInCart);
                let totalAmount = 0;

                userCart.product.forEach(element => {
                    totalAmount += element.total;
                });
                userCart.totalAmountOfAllProducts = totalAmount;
                await userCart.save();

                return res.status(200).json(userCart);
            }
        }                
        else{
            const newItemToCart = await Cart.create(addItemToCart);
            return res.status(200).json(newItemToCart);
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }    
}

const checkYourCart = async (req, res) => {
    try{
        const token = await req.headers.authorization;
        const userData = await auth.decode(token);
        const userCart = await Cart.findOne({userId: userData.id});

        if(userData.isAdmin) { return res.send("Admin is not allowed to have a cart!"); }

        if(!userCart){
            return res.status(404).send("You dont have any items in your cart!");
        }
        res.status(200).json(userCart)
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

//Admin Only
const checkAllCarts = async (req, res) => {
    try{
        const token = await req.headers.authorization;
        const userData = await auth.decode(token);
        const userCart = await Cart.find({})

        if(!userData.isAdmin) { res.send("Only admin can perform this action!"); }

        if(!userCart){
            return res.status(404).send("No one added a product in their cart yet");
        }
        res.status(200).json(userCart)
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

const editQuantity = async (req, res) => {
    try{
        const productId = req.body.productId;

        if(!ObjectId.isValid(productId)){
            return res.status(400).send(`Product Id ${productId} is not valid objectId!`);
        }

        const token = req.headers.authorization;
        const userData = auth.decode(token);
        const userCart = await Cart.findOne({userId: userData.id});
        let arrIndex;
        if(userCart){
            for(let i=0; i < userCart.product.length; i++){
                if (userCart.product[i].productId === req.body.productId){
                    arrIndex = i;
                    break;
                }
            }
            const userCartToBeUpdated = userCart.product[arrIndex];
            if (arrIndex >= 0){
                userCartToBeUpdated.quantity += req.body.quantity;
                if(userCartToBeUpdated.quantity < 1){
                    return userCart.product.splice(arrIndex, 1)
                }
                userCartToBeUpdated.total = userCartToBeUpdated.price * userCartToBeUpdated.quantity;
                let totalAmount = 0;
                userCart.product.forEach(element => {
                    totalAmount += element.total;
                });
                userCart.totalAmountOfAllProducts = totalAmount
                await userCart.save({new:true})
                
                return res.status(200).json(userCart)
            }
            else{
                return res.status(400).send("Sorry, The product you are trying to edit the quantities is not yet in your Cart. Please add first!")
            }
        }
        else{
            return res.status(400).send("Sorry, you dont have any product in your cart! Add products first!")
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

const removeProductFromCart = async (req, res) => {
    try{
        const productId = req.params.productId;
        
        if(!ObjectId.isValid(productId)){
            return res.status(400).send(`Product Id ${productId} is not valid objectId!`);
        }

        const token = req.headers.authorization
        const userData = auth.decode(token);

        const userCart = await Cart.findOne({userId: userData.id})
        let arrIndex;
        if(userCart){
            for(let i= 0; i < userCart.product.length; i++){
                if(userCart.product[i].productId === req.params.productId){
                    arrIndex = i;
                    break;
                }
            }
            const userCartToBeDeleted = userCart.product;
            if(arrIndex >= 0){
                userCartToBeDeleted.splice(arrIndex, 1)
                let totalAmount = 0;
                userCart.product.forEach(element => {
                    totalAmount += element.total;
                });
                userCart.totalAmountOfAllProducts = totalAmount
                await userCart.save({new:true})
                return res.status(200).send(userCart)
            }
            else{
                return res.status(400).send("Sorry, The product you are trying to remove is not yet in your Cart!")
            }
        }
        else{
            return res.status(400).send("Your cart is already empty")
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

const checkOutOrders = async (req, res) => {
    try{
        const token = req.headers.authorization;
        const userData = auth.decode(token);

        let userTotalSpent = 0;
        const userTrans = await OrderTransactionHistory.find({userId: userData.id})

        userTrans.forEach(element => {
            userTotalSpent += element.totalAmount
        })
        
        if(userTotalSpent > 50000 && userTotalSpent < 100000){
            const user = await User.findOne({_id: userData.id})
            user.userMembership = "gold"
            user.save()
        }
        else if(userTotalSpent > 100000){
            const user = await User.findOne({_id: userData.id})
            user.userMembership = "platinum"
            user.save()
        }
        
        if(userData.isAdmin) { return res.send("Admin is not allowed placed an order!") }
        
        else{
            const userCart = await Cart.findOne({userId: userData.id});
            const user = await User.findOne({_id: userData.id})
            let productToCheckOut = [];
            let totalAmountOfCheckout = 0;
            let arrIndex = [];
            // let arrIdexOfNotAvailable = [];

            if(!userCart) { return res.send("You have no items in your cart")} 
            else{
                for(let i=0; i < userCart.product.length; i++){

                    let product = await Product.findById({_id:userCart.product[i].productId});

                    if(product.isActive && product.stocks >= userCart.product[i].quantity && userCart.product[i].quantity > 0){
                        totalAmountOfCheckout += userCart.product[i].total;
                        await productToCheckOut.push(userCart.product[i]);
                        product.stocks += -userCart.product[i].quantity;
                        arrIndex.push(i);
                    }
                    // else if(!product.isActive && product.stocks < userCart.product[i].quantity){
                    //     arrIdexOfNotAvailable.push(i);
                    // }


                    if(product.stocks == 0)
                        product.isActive = false;

                    await product.save();
                }

                switch(user.userMembership){
                    case "gold":
                        totalAmountOfCheckout -= (totalAmountOfCheckout * 0.05)
                        break;
                    case "platinum":
                        totalAmountOfCheckout -= (totalAmountOfCheckout * 0.1)
                        break;
                }

                const completedOrders = await new OrderTransactionHistory({
                    userId: userData.id,
                    product: productToCheckOut,
                    totalAmount: totalAmountOfCheckout
                })

            
                if(productToCheckOut.length == 0) {res.send(`Sorry you don't have any items in cart that is available for checkout!`)}
                else{
                    const newCompleteOrders = await OrderTransactionHistory.create(completedOrders)
                    // console.log(arrIdexOfNotAvailable)
                    console.log(arrIndex.reverse())
                    
                    for(let i=0; i < arrIndex.length; i++){
                        await userCart.product.splice(arrIndex.reverse(), 1)
                        await userCart.save();
                    }
                    
                    console.log(userTotalSpent +totalAmountOfCheckout)
                    // let sucessfulOrderMessage = `Order sucessfully placed!\n${productToCheckOut.length} Products\nwith a total amount of ${totalAmountOfCheckout}\nHere is your receipt: ${newCompleteOrders._id}\n\n${productToCheckOut}\n\n\n`
                    // let concatMess = sucessfulOrderMessage += `There are ${arrIdexOfNotAvailable} product'(s) that is not included to your checkout due to unavailable or your quantinty is higher than the stocks!\n`
                    // let combineMess = sucessfulOrderMessage.concat(concatMess)
                    
                    // if(arrIdexOfNotAvailable.length > 0){
                    //     return res.status(200).send(combineMess);
                    // }
                    // else{
                    //     return res.status(200).send(sucessfulOrderMessage);
                    // }
                    res.status(200).send(`Order sucessfully placed!\n${productToCheckOut.length} Products\nwith a total amount of ${totalAmountOfCheckout}\nHere is your receipt: ${newCompleteOrders._id}\n\n${productToCheckOut}\n\n\n`)
                }
            }
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

const getAllUserTransaction = async (req, res) => {
    const token = req.headers.authorization;
    const userData = auth.decode(token);

        const userTrans = await OrderTransactionHistory.find({userId: userData.id})
        if(userTrans){
            return res.status(200).json(userTrans)
        }
        else{
            res.send("You dont make any checkout yet!")
        }
}


module.exports = {
    addProductToCart,
    editQuantity,
    removeProductFromCart,
    checkOutOrders,
    checkYourCart,
    checkAllCarts,
    getAllUserTransaction
}






