const User = require("../models/usersModel")
const bcrypt = require("bcrypt")
const auth = require("../auth")
const ObjectId = require('mongoose').Types.ObjectId;

const registerUser = async (req, res) => {

    const checkDuplicateEmail = await User.findOne({email : req.body.email});

    try{
        if(!checkDuplicateEmail){
            const user = new User({
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10)
            })
            try{
                const newUser = await User.create(user);
                res.status(200).json(newUser);
            }catch (err) {
                res.status(400).json({error: err.message});
            }
        }
        else{
            res.status(404).send("The email is already taken! Please choose another one.");
        }
    }catch(err){
        res.status(400).send({error: err.message});
    }
}


const getAllUsers = async (req, res) => {
    try{
        const token = req.headers.authorization
        const userData = auth.decode(token)
        const allUsers = await User.find({});
        if(userData.isAdmin)
            res.status(200).json(allUsers);
        else{
            res.status(400).send("Only admin can perform this action!")
        }
    }catch(err){
        res.status(400).send({error: err.message});
    }
}



const loginUser = async (req, res, next) => {
    try{
        const loginCreds = await User.findOne({email : req.body.email});

        if(!loginCreds){
            return res.status(404).send(`The email you entered is not yet registered. Please register first!`);
        }
        else{
            const isPasswordCorrect = await bcrypt.compareSync(req.body.password, loginCreds.password);

            if(isPasswordCorrect){
                res.status(200).json({accessToken: auth.createToken(loginCreds)});
            }
            else{
                res.status(404).send(`Password is incorrect!`);
            }
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }
}



const updateRole = async (req, res) => {
    try{
        const productId = req.params.id;
        if(!ObjectId.isValid(productId)){
            return res.status(400).send(`Product Id ${productId} is not valid objectId!`);
        }
        const token = req.headers.authorization
        const userData = auth.decode(token)
        const userTobeUpdated = req.params.id

        if(userData.isAdmin){
            const result = await User.findById(userTobeUpdated)
            let updateUser = {
                isAdmin: !result.isAdmin
            }
            const updatedUser = await User.findByIdAndUpdate(userTobeUpdated, updateUser, {new: true})
            if(updatedUser){
            res.status(200).json(updatedUser)
            }
            else{
                res.status(400).send("User not found!")
            }
        }
        else{
            res.status(400).send("You dont have acces on this page")
        }
    }catch(err){
        res.status(400).send({error : err.message})
    }
}

module.exports = {
    registerUser,
    updateRole,
    loginUser,
    getAllUsers
}