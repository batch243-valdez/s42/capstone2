const Product = require("../models/productsModel")
const auth = require("../auth");
const ObjectId = require('mongoose').Types.ObjectId;

const addProduct = async (req, res) => {
    try{
        const token = req.headers.authorization;
        const userData = auth.decode(token);

        if(userData.isAdmin){
            const checkDuplicateProduct = await Product.findOne({name: req.body.name})
            if(!checkDuplicateProduct){
                const product = await new Product({
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price,
                    stocks: req.body.stocks
                })
                product.isActive = product.stocks == 0 ? false : true;
                const newProduct = await Product.create(product)
                return res.status(200).json(newProduct)
            }
            res.status(404).send("The product you entered is already exist!")
        }
        res.status(400).send("Only admin can add a products!")
    }catch(err){
        res.status(400).send({error: err.message})
    }
}

const getProduct = async (req, res) => {
    try{
        const product = await Product.findOne({_id: req.params.productId})
        if(product){
            if(product.isActive){
                return res.status(200).json(product)
            }
            else{
                res.send("Only admin allowed to do this!")
            }
        }
        res.status(400).send("The product you are trying to search is not existing!")
    }catch(err){
        res.status(400).send({error: err.message})
    }
}

const getProductByName = async (req, res) => {
    try{
        const product = await Product.findOne({name: req.body.name})
        if(product){
            if(product.isActive){
                return res.status(200).json(product)
            }
            else{
                res.send("Only admin allowed to do this!")
            }
        }
        res.status(400).send("The product you are trying to search is not existing!")
    }catch(err){
        res.status(400).send({error: err.message})
    }
}


const allProducts = async (req, res) => {
    try{
        const token = req.headers.authorization;
        const userData = auth.decode(token);

        if(userData.isAdmin){
            const allProducts = await Product.find({});
            res.status(200).json(allProducts);
        }
        else{
            res.status(400).send("Only admin is allowed to do this action.")
        }
    }catch(err){
        res.status(400).send({error: err.message})
    }
}


const allActiveProducts = async(req, res) => {
    try{
        const token = req.headers.authorization;
        const userData = auth.decode(token);

        if(userData.isAdmin){
            const activeProducts = await Product.find({isActive : true})
            return res.status(200).json(activeProducts)
        }
        res.status(400).send("Only admin can retrive all active products!")
    }catch(err){
        res.status(400).json({error: err.message})
    }
}

const updateProducts = async (req, res) => {
    try{
        const productId = req.params.productId;
        if(!ObjectId.isValid(productId)){
            return res.status(400).send(`Product Id ${productId} is not valid objectId!`);
        }

        const token = req.headers.authorization;
        const userData = auth.decode(token)
        if(userData.isAdmin){
            const productToUpdate = req.params.productId

            const updatedProduct = await Product.findOneAndUpdate({_id: productToUpdate}, {
                ...req.body
            }, { new : true})

            updatedProduct.isActive = updatedProduct.stocks == 0 ? false : true;
            await updatedProduct.save();
            if(updatedProduct){
                return res.status(200).json(updatedProduct)
            }
            res.status(400).send("Sorry, The product you are trying to update is not existing!")
        }
        res.status(400).send("Sorry, But only admin is allowed to update a products!")
    }catch(err){
        res.status(400).json({error: err.message})
    }
}

const archiveProduct = async (req, res) => {
    try{
        const token = req.headers.authorization;
        const userData = auth.decode(token)
    

        if(userData.isAdmin){
            const productData = await Product.findOne({name: req.body.name})

            const productToArchive = await Product.findOneAndUpdate({name: req.body.name}, {isActive: !productData.isActive}, {new: true})
            if(productToArchive){
                return res.status(400).json(productToArchive)
            }
        }
    }catch(err){
        res.status(400).send("Sorry, The product you are trying to archived is not existing!")
    }
}


module.exports = {
    addProduct,
    allActiveProducts,
    allProducts,
    updateProducts,
    archiveProduct,
    getProduct,
    getProductByName
}