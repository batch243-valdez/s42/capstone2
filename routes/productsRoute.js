const express = require("express");
const auth = require("../auth")

const router = express.Router();

const {
    addProduct,
    allActiveProducts,
    allProducts,
    updateProducts,
    archiveProduct,
    getProduct,
    getProductByName
} = require("../controllers/productController")

router.post("/addProducts", auth.verify, addProduct)
router.get("/allActiveProducts", allActiveProducts)
router.get("/allProducts", allProducts)
router.get("/specificProduct", getProductByName)
router.get("/specificProduct/:productId", getProduct)
router.patch("/updateProducts/:productId", auth.verify, updateProducts)
router.patch("/archiveProducts", auth.verify, archiveProduct)

module.exports = router;