const express = require("express");
const auth = require("../auth");

const router = express.Router();

const {
    addProductToCart,
    editQuantity,
    removeProductFromCart,
    checkOutOrders,
    checkYourCart,
    checkAllCarts,
    getAllUserTransaction
} = require("../controllers/cartController")

router.post("/addToCart", auth.verify, addProductToCart)
router.put("/editProductQuantity", auth.verify, editQuantity)
router.delete("/removeProduct/:productId", auth.verify, removeProductFromCart)
router.post("/checkOutOrder", auth.verify, checkOutOrders)
router.get("/checkYourCart", auth.verify, checkYourCart)
router.get("/checkAllCarts", auth.verify, checkAllCarts)
router.get("/checkTransactions", auth.verify, getAllUserTransaction)

module.exports = router;
