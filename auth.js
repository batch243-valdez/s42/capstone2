const jwt = require("jsonwebtoken");

const secret = "EcommerceAPI"


const createToken = (loginCreds) => {
    const data = {
        id: loginCreds._id,
        email: loginCreds.email,
        isAdmin: loginCreds.isAdmin,
        userMembership: loginCreds.userMembership
    }
    return jwt.sign(data, secret, {})
}


const verify = (req, res, next) => {

    let token = req.headers.authorization;
    console.log(token);
    if(token){
        token = token.split(" ")[1];
        return jwt.verify(token, secret, (error) => {
            if(error){
                res.status(404).json("Invalid Token");
            }
            else{
                next();
            }
        })
    }
    else{
        res.status(404).json("Authentication failed! No Token provided")
    }
}

const decode = (token) => {
    if(token === undefined){
        return null
    }
    else{
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (error, data) => {
            if(error){
                return null;
            }else{
                return jwt.decode(token, {complete: true}).payload
            }
        })
    }
}


module.exports = {
    createToken,
    verify,
    decode
}