const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const orderTransactionHistorySchema = new Schema({
    userId: {
        type: Object,
        required: [true, "User ID is required"]
    },
    product:[
        {
            productId: {
                type: Object,
                required: [true, "Product id is required"]
            },
            productName: {
                type: String,
                required: [true, "Product name is required"]
            },
            price: {
                type: Number,
                required: [true, "Product price is required"]
            },
            quantity: {
                type: Number,
                required: [true, "Product quantity is required"]
            },
            total: {
                type: Number,
                required: [true, "Total price of this product is required"]
            }
        }
    ], 
    totalAmount:{
        type: Number,
        required: [true, "TotalAmount is required"]
    }
}, {timestamps: true})

module.exports = mongoose.model("OrderTransactionHistory", orderTransactionHistorySchema);