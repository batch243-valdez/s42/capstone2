const mongoose = require("mongoose");

const Schema = mongoose.Schema;

    // let b = new Date();
    // let utc = b.getTime() + (b.getTimezoneOffset()*60000);
    // let nd = new Date(utc+(3600000* +8));

const userSchema = new Schema({
    email: {
        type: String,
        required: [true, "Email is required"]
    },
    password: {
        type: String,
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    userMembership: {
        type: String,
        default: "regular"
    },
    date: {
        type: Date,
        default: new Date().toLocaleDateString('en-PH')
    },
    time: {
        type: Date,
        default: new Date().toLocaleTimeString()
    }
}, {timestamps: true});

module.exports = mongoose.model("User", userSchema);