// dependencies
require("dotenv").config()
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors");

const cartsRoute = require("./routes/cartsRoute")
const productsRoute = require("./routes/productsRoute")
const usersRoute = require("./routes/usersRoute")
const app = express();

// middlewares
app.use(cors());
app.use(express());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// connect to database
mongoose.set("strictQuery", true)
mongoose.connect("mongodb+srv://admin:admin@zuittbatch243.eiw7ahh.mongodb.net/Capstone2EcommerceAPI?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => {
        app.listen(process.env.PORT || 4000, () => {
            console.log(`Connected to database & port`)
        })
    }).catch((error) => {
        console.log(error)
    })


app.use("/users", usersRoute);
app.use("/products", productsRoute);
app.use("/cart", cartsRoute);